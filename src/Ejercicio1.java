import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Ejercicio1 {
    public static void main(String[] args) throws IOException {
        //Creamos los procesos padre e hijo y asignamos el comando que añadimos en los argumentos(args).
        List<String> argsList = new ArrayList<>(Arrays.stream(args).toList());
        ProcessBuilder pb1 = new ProcessBuilder(argsList);
        Process p = pb1.start();
        //Calculamos el tiemop que tarda y si se pasa devuelve un true con el InterruptedIOException y si devuelve false se ejecuta normal.
        try {
            if(!p.waitFor(2, TimeUnit.SECONDS)){
                throw new InterruptedIOException();
            }
        } catch (InterruptedException e) {
            System.out.println("Tiempo de espera agotado");
            System.exit(-1);
        }
        //Iniciamos los Imputs y Buffer para poder leer el comando y escribilo en el fichero.
        InputStream is = p.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String salidaCmd;
        BufferedWriter bw = new BufferedWriter(new FileWriter("output.txt"));
        String escrituraCmd= null;
        //Imprimo lo que devuelve el comando y lo escribo en el fichero.
        try{
            while ((salidaCmd = br.readLine()) != null) {
                escrituraCmd += salidaCmd+"\n";
            }
            System.out.println(escrituraCmd);
            bw.write(escrituraCmd);
            bw.close();
        }catch (Exception e) {
            System.out.println("El hijo ha fallado");
        }
    }
}
