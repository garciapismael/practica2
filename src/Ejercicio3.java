import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio3 {
    public static void main(String[] args) throws IOException {
        //Llama a Random10.jar.
        String command = "java -jar out\\artifacts\\Minusculas_jar\\Minusculas.jar";
        List<String> argList = new ArrayList<>(Arrays.asList(command.split(" ")));
        //Crea los processos.
        ProcessBuilder pb = new ProcessBuilder(argList);
        Process process = pb.start();

        OutputStream os = process.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);
        //Scaner para leer lo que imprime la clase Minusculas
        Scanner procesoSC = new Scanner(process.getInputStream());
        Scanner sc = new Scanner(System.in);
        String cadena = sc.nextLine().toLowerCase();
        String cadenaMinusculas = "";

        try{
            //compara la cadena que has escrito y si es diferente de finalizar sigue ejecutando.
            while(!cadena.equals("finalizar")){
                bw.write(cadena + "\n");
                //vacia la linea por si hay algo.
                bw.flush();
                //devuelve la cadena en minusculas
                cadenaMinusculas = procesoSC.nextLine();
                System.out.println(cadenaMinusculas);
                cadena = sc.nextLine().toLowerCase();
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
