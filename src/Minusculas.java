import java.util.Scanner;

public class Minusculas {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String cadena = sc.nextLine().toLowerCase();

        while(!cadena.equals("finalizar")){
            System.out.println(cadena);
            cadena = sc.nextLine().toLowerCase();
        }
    }
}
